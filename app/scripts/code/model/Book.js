var Book = Backbone.Model.extend({
    defaults: {
        ID:'',
        author:'',
        title:'',
        genre:'',
        notes:''
    },
    initialize: function() {
        console.log("Book init");
    },
    constructor: function (attrbs, opts){
        Backbone.Model.apply(this, options)

    },
    validate: function(attrb){
        if(!attrb.author || !attrb.title){
            return false;
        }
        return true;
    }
});