require.config({
    paths: {
        "jquery": "libs/jquery/dist/jquery",
        "underscore": "libs/underscore-amd/underscore",
        "backbone": "libs/backbone-amd/backbone",
        "bootstrap": "libs/bootstrap/dist/js/bootstrap.js"
    }
});

require(["code/views/app"], function(App){
    new App;
});